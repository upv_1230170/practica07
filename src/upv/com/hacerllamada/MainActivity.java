package upv.com.hacerllamada;

import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	Button save,load;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		save = (Button)findViewById(R.id.button1);
		load = (Button)findViewById(R.id.button2);
		save.setOnClickListener(new View.OnClickListener() {
			 
			@Override

			 public void onClick(View view) {
				EditText num=(EditText)findViewById(R.id.editText1); 
		        String number = "tel:" + num.getText().toString().trim();
		        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
		        startActivity(callIntent);
				}
			 });
		load.setOnClickListener(new View.OnClickListener() {
			 
			@Override

			 public void onClick(View view) {
				EditText msj=(EditText)findViewById(R.id.editText2); 
				EditText num=(EditText)findViewById(R.id.editText3);
				String number = num.getText().toString();
				String message = msj.getText().toString();
				SmsManager manager = SmsManager.getDefault();
				manager.sendTextMessage(number,null,message,null,null);
				Toast.makeText(getApplicationContext(),"Mensaje Enviado", Toast.LENGTH_LONG).show();
				}
			 });
	}


	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

}
